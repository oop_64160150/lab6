package com.anawin.week6;

public class BookBank {
    
    //Attributes
    private String name;
    private double balance;
    public BookBank(String name,double balanece){//Constructor
        this.name = name;
        this.balance = balanece;
    }
    //Methods
    public boolean deposit(double money){
        if(money<1) return false;
        balance = balance + money;
        return true;
    }
    public boolean withdraw(double money){
        if(money>balance) return false;
        if(money <1) return false;
        balance = balance - money;
        return true;
    }
    public void print(){
        System.out.println(name + " " + balance);
    }
    
    public String getName(){
        return name;
    }

    public double getBalance(){
        return balance;
    }
}
