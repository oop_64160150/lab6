package com.anawin.week6;

public class RobotApp {
    public static void main(String[] args) {
        Robot Kapong = new Robot("Kapong",'K',0,0);
        Kapong.print();
        for(int i = 0 ; i<10;i++){
            Kapong.right();
            Kapong.down();
        }
        Kapong.print();

        Robot Blue = new Robot("Blue",'B',1,1);
        Blue.print();
    }
}
