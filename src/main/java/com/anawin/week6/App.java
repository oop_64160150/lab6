package com.anawin.week6;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        BookBank anawin =  new BookBank("Anawin",10000.0);//constructure
        anawin.print();
        anawin.withdraw(500.0);
        anawin.print();
        anawin.deposit(500.0);
        anawin.print();

        BookBank Sia = new BookBank("Sia",100000.0);
        Sia.print();
        Sia.withdraw(5000.0);
        Sia.print();
        Sia.deposit(1.0);
        Sia.print();



    }
}
