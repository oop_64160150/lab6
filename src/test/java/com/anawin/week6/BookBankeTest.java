package com.anawin.week6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BookBankeTest {
    @Test
    public void shouldWithdrawSuccess(){
        BookBank book = new BookBank("Anawin",100);
        book.withdraw(50);
        assertEquals(50.0,book.getBalance(),0.0001);
    }

    @Test
    public void shouldWithdrawOverBalance(){
        BookBank book = new BookBank("Anawin",100);
        book.withdraw(150);
        assertEquals(100,book.getBalance(),0.0001);
    }

    @Test
    public void shouldWithdrawWithNegativeNumber(){
        BookBank book = new BookBank("Anawin",100);
        book.withdraw(-100);
        assertEquals(100,book.getBalance(),0.0001);
    }

    @Test
    public void shouldDepositSuccess(){
        BookBank book = new BookBank("Anawin",100);
        book.deposit(50);
        assertEquals(150.0,book.getBalance(),0.0001);
    }
    
    @Test
    public void shouldWithDepositWithNegativeNumber(){
        BookBank book = new BookBank("Anawin",100);
        book.deposit(-100);
        assertEquals(100,book.getBalance(),0.0001);
    }
}